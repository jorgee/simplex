<?php

namespace simplex\interfaces;

use Exception;

interface CurrencyConverterInterface {

    /**
     * @param string $from ISO-4217 code
     * @param string $to ISO-4217 code
     * @param float $value
     * @throws Exception If conversion rate $from -> $to is not found.
     *
     * @return float
     */
    public function convertValue(string $from, string $to, float $value) : float;

    /**
     * @return CurrencyConversionRateInterface[]
     */
    public function getConversionRates() : array;

    /**
     * @param string $fromCurrencyCode ISO-4217 3-char code.
     * @param string $toCurrencyCode ISO-4217 3-char code.
     *
     * @return CurrencyConversionRateInterface|null The currency conversion rate object or null if not found.
     */
    public function getCurrencyConversionRate(string $fromCurrencyCode, string $toCurrencyCode) : ? CurrencyConversionRateInterface;


    /**
     * @param CurrencyConversionRateInterface $conversionRate
     *
     * @return mixed
     */
    public function addCurrencyConversionRate(CurrencyConversionRateInterface $conversionRate);

}