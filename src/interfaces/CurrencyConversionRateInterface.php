<?php

namespace simplex\interfaces;

interface CurrencyConversionRateInterface {

    /**
     * @return string The ISO-4217 3-char currency code of the origin currency
     */
    public function getFromCurrencyCode() : string;

    /**
     * @return string The ISO-4217 3-char currency code of the destiny currency
     */
    public function getToCurrencyCode() : string;

    /**
     * @return float The conversion rate between currencies.
     */
    public function getConversionRate() : float;
}