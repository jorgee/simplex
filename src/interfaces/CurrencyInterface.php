<?php

namespace simplex\interfaces;

interface CurrencyInterface {

    public function setCurrencyConverter(CurrencyConverterInterface $currencyConverter) : void;
    public function getCurrencyConverter() : CurrencyConverterInterface;
    public function add(CurrencyInterface $currency) : CurrencyInterface;
    public function subtract(CurrencyInterface $currency) : CurrencyInterface;
    public function getValue() : float;
    public function getCode() : string;

}