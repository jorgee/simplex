<?php

namespace simplex;

use Exception;
use simplex\interfaces\CurrencyConverterInterface;
use simplex\interfaces\CurrencyInterface;

class Currency implements CurrencyInterface {

    /** @var CurrencyConverterInterface */
    protected $currencyConverter;

    /** @var float */
    protected $value = 0;

    /** @var string ISO-4217 3-chars code */
    protected $code;

    /**
     * Currency constructor.
     *
     * @param string $code
     * @param CurrencyConverterInterface $currencyConverter
     * @param float $value
     */
    public function __construct(string $code, CurrencyConverterInterface $currencyConverter, float $value = 0) {
        $this->currencyConverter = $currencyConverter;
        $this->code = $code;
        $this->value = $value;
    }

    /**
     * @return CurrencyConverterInterface
     */
    public function getCurrencyConverter(): CurrencyConverterInterface {
        return $this->currencyConverter;
    }

    /**
     * @param CurrencyConverterInterface $currencyConverter
     */
    public function setCurrencyConverter(CurrencyConverterInterface $currencyConverter): void {
        $this->currencyConverter = $currencyConverter;
    }

    /**
     * @return float
     */
    public function getValue(): float {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getCode(): string {
        return $this->code;
    }

    /**
     * @param CurrencyInterface $currency
     *
     * @return CurrencyInterface
     * @throws Exception
     */
    public function add(CurrencyInterface $currency) : CurrencyInterface {
        $value = $this->currencyConverter->convertValue($currency->getCode(), $this->getCode(), $currency->getValue());
        $this->value += $value;
        return $this;
    }

    /**
     * @param CurrencyInterface $currency
     *
     * @return CurrencyInterface
     * @throws Exception
     */
    public function subtract(CurrencyInterface $currency) : CurrencyInterface {
        $value = $this->currencyConverter->convertValue($currency->getCode(), $this->getCode(), $currency->getValue());
        $this->value -= $value;
        return $this;
    }
}