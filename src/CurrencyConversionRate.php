<?php

namespace simplex;

use simplex\interfaces\CurrencyConversionRateInterface;

class CurrencyConversionRate implements CurrencyConversionRateInterface {

    /** @var string ISO-4217 3-char currency code */
    protected $fromCurrencyCode;

    /** @var string ISO-4217 3-char currency code */
    protected $toCurrencyCode;

    /** @var float The conversion rate */
    protected $conversionRate;

    /**
     * CurrencyConversionRate constructor.
     *
     * @param $fromCurrencyCode
     * @param $toCurrencyCode
     * @param $conversionRate
     */
    public function __construct(string $fromCurrencyCode, string $toCurrencyCode, float $conversionRate) {
        $this->fromCurrencyCode = $fromCurrencyCode;
        $this->toCurrencyCode = $toCurrencyCode;
        $this->conversionRate = $conversionRate;
    }

    /**
     * @inheritDoc
     */
    public function getFromCurrencyCode(): string {
        return $this->fromCurrencyCode;
    }

    /**
     * @inheritDoc
     */
    public function getToCurrencyCode(): string {
        return $this->toCurrencyCode;
    }

    /**
     * @inheritDoc
     */
    public function getConversionRate(): float {
        return $this->conversionRate;
    }
}