<?php

namespace simplex;

use Exception;
use simplex\interfaces\CurrencyConversionRateInterface;
use simplex\interfaces\CurrencyConverterInterface;

class CurrencyConverter implements CurrencyConverterInterface {

    /** @var CurrencyConversionRateInterface[] */
    protected $currencyConversionRates = [];

    /**
     * @inheritDoc
     */
    public function convertValue(string $from, string $to, float $value) : float {
        if ($from === $to) {
            return $value;
        }
        $currencyConversionRate = $this->getCurrencyConversionRate($from, $to);
        if (!$currencyConversionRate) {
            throw new Exception(sprintf('Could not find conversion rate from currency %s to %s', $from, $to));
        }
        $rate = $currencyConversionRate->getConversionRate();
        return $value * $rate;
    }

    /**
     * @inheritDoc
     */
    public function getConversionRates(): array {
        return $this->currencyConversionRates;
    }

    /**
     * @inheritDoc
     */
    public function addCurrencyConversionRate(CurrencyConversionRateInterface $conversionRate) {
        $from = $conversionRate->getFromCurrencyCode();
        $to = $conversionRate->getToCurrencyCode();
        $this->currencyConversionRates[$from][$to] = $conversionRate;
    }

    /**
     * @inheritDoc
     */
    public function getCurrencyConversionRate(string $fromCurrencyCode, string $toCurrencyCode) : ? CurrencyConversionRateInterface {
        if (isset($this->currencyConversionRates[$fromCurrencyCode][$toCurrencyCode])) {
            return $this->currencyConversionRates[$fromCurrencyCode][$toCurrencyCode];
        }
        return null;
    }
}
