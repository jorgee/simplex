<?php

namespace simplex\tests\unitary\currencyConversionRate;

use PHPUnit\Framework\TestCase;
use simplex\CurrencyConversionRate;
use simplex\interfaces\CurrencyConversionRateInterface;

final class CurrencyConversionRateInstantiateTest extends TestCase {

    public function testCurrencyConversionRateCanBeInstantiated(): void {
        $currencyConversionRate = new CurrencyConversionRate('ARS', 'USD', 0.023255814);
        $this->assertInstanceOf(
            CurrencyConversionRate::class,
            $currencyConversionRate
        );
    }

    public function testCurrencyConversionRateImplementsInterface(): void {
        $currencyConversionRate = new CurrencyConversionRate('ARS', 'USD', 0.023255814);
        $this->assertInstanceOf(
            CurrencyConversionRateInterface::class,
            $currencyConversionRate
        );
    }

    public function testCurrencyConversionRatePropertiesAreOnesGivenInConstructor(): void {
        $currencyConversionRate = new CurrencyConversionRate('ARS', 'USD', 0.023255814);
        $this->assertEquals(
            'ARS',
            $currencyConversionRate->getFromCurrencyCode()
        );
        $this->assertEquals(
            'USD',
            $currencyConversionRate->getToCurrencyCode()
        );
        $this->assertEquals(
            0.023255814,
            $currencyConversionRate->getConversionRate()
        );
    }
}
