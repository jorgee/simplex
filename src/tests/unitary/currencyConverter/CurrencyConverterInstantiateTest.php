<?php

namespace simplex\tests\unitary\currencyConverter;

use PHPUnit\Framework\TestCase;
use simplex\CurrencyConverter;
use simplex\interfaces\CurrencyConverterInterface;

final class CurrencyConverterInstantiateTest extends TestCase {

    public function testCurrencyConverterCanBeInstantiated(): void {
        $currencyConverter = new CurrencyConverter();
        $this->assertInstanceOf(
            CurrencyConverter::class,
            $currencyConverter
        );
    }

    public function testCurrencyConverterImplementsInterface(): void {
        $currencyConverter = new CurrencyConverter();
        $this->assertInstanceOf(
            CurrencyConverterInterface::class,
            $currencyConverter
        );
    }
}