<?php

namespace simplex\tests\unitary\currencyConverter;

use PHPUnit\Framework\TestCase;
use simplex\CurrencyConverter;
use simplex\interfaces\CurrencyConversionRateInterface;

final class CurrencyConverterConvertTest extends TestCase {

    public function testCurrencyIsConvertedCorrectly(): void {
        $mockedCurrencyConversionRate = $this->createMock(CurrencyConversionRateInterface::class);
        $mockedCurrencyConversionRate
            ->method('getConversionRate')
            ->willReturn(43);
        $mockedCurrencyConversionRate
            ->method('getFromCurrencyCode')
            ->willReturn('USD');
        $mockedCurrencyConversionRate
            ->method('getToCurrencyCode')
            ->willReturn('ARS');
        $currencyConverter = new CurrencyConverter();
        $currencyConverter->addCurrencyConversionRate($mockedCurrencyConversionRate);
        $convertedValue = $currencyConverter->convertValue('USD', 'ARS', 1);
        $this->assertEquals(
            43,
            $convertedValue
        );
    }

    public function testExceptionIsThrownWhenNoConversionInfoAvailable(): void {
        $mockedCurrencyConversionRate = $this->createMock(CurrencyConversionRateInterface::class);
        $mockedCurrencyConversionRate
            ->method('getConversionRate')
            ->willReturn(43);
        $mockedCurrencyConversionRate
            ->method('getFromCurrencyCode')
            ->willReturn('USD');
        $mockedCurrencyConversionRate
            ->method('getToCurrencyCode')
            ->willReturn('ARS');
        $currencyConverter = new CurrencyConverter();
        $currencyConverter->addCurrencyConversionRate($mockedCurrencyConversionRate);
        $this->expectException(\Exception::class);
        $currencyConverter->convertValue('USD', 'EUR', 1);
    }
}