<?php

namespace simplex\tests\unitary\currencyConverter;

use PHPUnit\Framework\TestCase;
use simplex\CurrencyConverter;
use simplex\interfaces\CurrencyConversionRateInterface;

final class CurrencyConverterAddCurrencyConversionRate extends TestCase {

    public function testCurrencyConversionRateIsAddedCorrectly(): void {
        $from = 'USD';
        $to = 'ARS';
        $mockedCurrencyConversionRate = $this->createMock(CurrencyConversionRateInterface::class);
        $mockedCurrencyConversionRate
            ->method('getConversionRate')
            ->willReturn(43);
        $mockedCurrencyConversionRate
            ->method('getFromCurrencyCode')
            ->willReturn($from);
        $mockedCurrencyConversionRate
            ->method('getToCurrencyCode')
            ->willReturn($to);
        $currencyConverter = new CurrencyConverter();
        $currencyConverter->addCurrencyConversionRate($mockedCurrencyConversionRate);
        $this->assertEquals(
            1,
            count($currencyConverter->getConversionRates())
        );
        $this->assertSame(
            $mockedCurrencyConversionRate,
            $currencyConverter->getCurrencyConversionRate($from, $to)
        );
        $this->assertSame(
            $mockedCurrencyConversionRate,
            $currencyConverter->getConversionRates()[$from][$to]
        );
    }
}