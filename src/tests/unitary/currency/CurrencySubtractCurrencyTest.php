<?php

namespace simplex\tests\unitary\currency;

use PHPUnit\Framework\TestCase;
use simplex\Currency;
use simplex\CurrencyConverter;

final class CurrencySubtractCurrencyTest extends TestCase {

    public function testCurrencyIsAddedCorrectly() : void {
        $currencyConverterMock = $this->createMock(CurrencyConverter::class);
        $currencyConverterMock->method('convertValue')
            ->willReturn(1);
        $currency = new Currency('ARS', $currencyConverterMock, 10);
        $currency2 = new Currency('ARS', $currencyConverterMock, 1);
        $currency = $currency->subtract($currency2);
        $this->assertEquals(
            9,
            $currency->getValue()
        );
    }
}
