<?php

namespace simplex\tests\unitary\currency;

use PHPUnit\Framework\TestCase;
use simplex\Currency;
use simplex\CurrencyConverter;

final class CurrencyAddCurrencyTest extends TestCase {

    public function testCurrencyIsAddedCorrectly() : void {
        $currencyConverterMock = $this->createMock(CurrencyConverter::class);
        $currencyConverterMock->method('convertValue')
            ->willReturn(43);
        $currency = new Currency('ARS', $currencyConverterMock, 1);
        $currency2 = new Currency('USD', $currencyConverterMock, 1);
        $currency = $currency->add($currency2);
        $this->assertEquals(
            44,
            $currency->getValue()
        );
    }
}
