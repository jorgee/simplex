<?php

namespace simplex\tests\unitary\currency;

use PHPUnit\Framework\TestCase;
use simplex\Currency;
use simplex\CurrencyConverter;
use simplex\interfaces\CurrencyInterface;

final class CurrencyInstantiateTest extends TestCase {

    public function testCurrencyCanBeInstantiated(): void {
        $currencyConverterMock = $this->createMock(CurrencyConverter::class);
        $currency = new Currency('ARS', $currencyConverterMock, 1);
        $this->assertInstanceOf(
            Currency::class,
            $currency
        );
    }

    public function testCurrencyImplementsInterface(): void {
        $currencyConverterMock = $this->createMock(CurrencyConverter::class);
        $currency = new Currency('ARS', $currencyConverterMock, 1);
        $this->assertInstanceOf(
            CurrencyInterface::class,
            $currency
        );
    }

    public function testCurrencyPropertiesAreOnesGivenInConstructor(): void {
        $currencyConverterMock = $this->createMock(CurrencyConverter::class);
        $currency = new Currency('ARS', $currencyConverterMock, 1);
        $this->assertEquals(
            'ARS',
            $currency->getCode()
        );
        $this->assertEquals(
            1,
            $currency->getValue()
        );
        $this->assertSame(
            $currencyConverterMock,
            $currency->getCurrencyConverter()
        );
    }
}
